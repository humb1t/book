# Dev Tricks

This chapter list all kinds of patterns and code you might need during developing veloren or compiling it from source.
It also features a list of common mistakes and how to detect and avoid them.
