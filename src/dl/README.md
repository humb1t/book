# Download

Veloren is a cross-platform game and can run on Windows, Linux and Mac OS.

If you're only interested in binary builds of the game, check the website at <https://www.veloren.net>.

(We currently don't yet provide official downloads of veloren, you have to download the source code, compile it yourself and run a server and client locally, see the next chapter [COMPILING](./compiling.md))
