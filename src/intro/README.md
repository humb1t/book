
# Veloren: An Owner's Manual

This manual is the primary source of documentation for Veloren, both for users and for developers.
It aims to document the features of Veloren, the manner in which it is developed, and describe the inner workings of both the game and the engine.
It also aims to discuss future plans the development team has for Veloren.
